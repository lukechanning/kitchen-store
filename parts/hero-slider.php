<div class="hero-slider">
  <?php
  //Let's call up our query to pull posts in the "Featured" category
    query_posts( array ( 'category_name' => 'featured', 'posts_per_page' => -1, 'order' => 'ASC' ) );
      //Make sure there are posts to get
      while (have_posts()) : the_post();
      $permalink = get_permalink();  //Get the permalink
        //If it's got a thumbnail, let's get that custom photo
        if (has_post_thumbnail( $post->ID ) ): ?>
          <?php $image = wp_get_attachment_image_src( get_post_thumbnail_id( $post->ID ), 'single-post-thumbnail' ); ?>
           <a href="<?php echo esc_url($permalink); ?>">
              <div class="hero item" style="background-image: url('<?php echo $image[0]; ?>')">
                <?php else: ?>
                <div class="hero item">
                <?php endif; ?>
              </div>
          </a>
        <?php
      endwhile;
      //Let's reset the query just in case
  wp_reset_query();
  ?>
</div>

<script type="text/javascript">
    jQuery(document).ready(function() {
	  	jQuery('.hero-slider').slick({
	  	  slidesToShow: 1,
        autoplay: true,
        autoplaySpeed: 5000,
	  	  arrows: false,
	  	  dots: false,
	  	  });
	  });
</script>